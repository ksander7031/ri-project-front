import React, { useEffect, useState } from 'react';
import axios from 'axios';

function App() {
  const [familyMembers, setFamilyMembers] = useState([]);

  useEffect(() => {
    fetchFamilyMembers();
  }, []);

  const fetchFamilyMembers = async () => {
    try {
      const response = await axios.get('http://localhost:8000/family-members');
      setFamilyMembers(response.data);
    } catch (error) {
      console.error('Failed to fetch family members:', error);
    }
  };

  const renderFamilyGraph = (members, parentId = null) => {
    const filteredMembers = members.filter((member) => member.parent_id === parentId);

    if (filteredMembers.length === 0) {
      return null;
    }

    return (
      <div className="family-graph">
        {filteredMembers.map((member) => (
          <div className="member-card" key={member.id}>
            <span className="member-name">{member.name}</span>
            <div className="connecting-line" />
            {renderFamilyGraph(members, member.id)}
          </div>
        ))}
      </div>
    );
  };

  return (
    <div className="app">
      <h1>Family Graph</h1>
      {renderFamilyGraph(familyMembers)}
      <style>{`
        .app {
          text-align: center;
          margin-top: 20px;
        }
      
        .family-graph {
          display: flex;
          flex-wrap: wrap;
          justify-content: center;
          gap: 20px;
          margin-top: 20px;
        }
      
        .member-card {
          background-color: #f9f9f9;
          padding: 10px;
          border-radius: 5px;
          box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
        }
      
        .member-name {
          font-weight: bold;
        }
      
        .connecting-line {
          width: 1px;
          background-color: #ccc;
          height: 20px;
          margin: 0 auto;
        }
      `}</style>
    </div>
  );
}

export default App;
